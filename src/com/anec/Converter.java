package com.anec;

public class Converter {

    private static final double INCH = 2.54;

    public static double InchesToCentimeters(double value) {
        return value * INCH;
    }

    public static double CentimetersToInches(double value) {
        return value / INCH;
    }
}
