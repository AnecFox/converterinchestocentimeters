package com.anec;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class MainWindow extends JFrame {

    private static final String VERSION = "0.1.2";

    private final JTextField textFieldValue = new JTextField();
    private final JTextField textFieldResult = new JTextField();

    private final JRadioButton radioButtonInchesToCentimeters = new JRadioButton("Дюймы в сантиметры");
    private final JRadioButton radioButtonCentimetersToInches = new JRadioButton("Сантиметры в дюймы");

    private final JButton buttonConvert = new JButton("Перевести");
    private final JButton buttonAbout = new JButton("О программе");

    public MainWindow() {
        initialize();
    }

    private void initialize() {
        setTitle("Inches To Centimeters");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(512, 138);
        setLocationRelativeTo(null);
        setResizable(false);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(3, 2, 5, 5));

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Не удалось поставить системный стиль",
                    this.getTitle(), JOptionPane.ERROR_MESSAGE);
        }

        textFieldValue.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    buttonConvert.doClick();
                }
            }
        });
        container.add(textFieldValue);

        textFieldResult.setEditable(false);
        container.add(textFieldResult);

        ButtonGroup group = new ButtonGroup();
        group.add(radioButtonInchesToCentimeters);
        group.add(radioButtonCentimetersToInches);

        radioButtonInchesToCentimeters.setSelected(true);
        container.add(radioButtonInchesToCentimeters);
        container.add(radioButtonCentimetersToInches);

        buttonConvert.addActionListener(e -> {
            String stringValue = textFieldValue.getText().contains(",") ?
                    textFieldValue.getText().replace(',', '.').trim() :
                    textFieldValue.getText().trim();

            if (!stringValue.isEmpty()) {
                double value;
                double result;

                String stringResult;

                try {
                    value = Double.parseDouble(stringValue);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Значение введено неправильно", getTitle(),
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                result = radioButtonInchesToCentimeters.isSelected() ? Converter.InchesToCentimeters(value) :
                        Converter.CentimetersToInches(value);

                stringResult = String.valueOf(result).replace('.', ',');

                textFieldResult.setText(stringResult.endsWith(",0") ? stringResult.replace(",0", "") :
                        stringResult);
            } else {
                JOptionPane.showMessageDialog(null, "Значение не введено", getTitle(),
                        JOptionPane.ERROR_MESSAGE);
            }
        });
        container.add(buttonConvert);

        buttonAbout.addActionListener(e -> JOptionPane.showMessageDialog(null,
                "Эта программа предназначена для конвертации\n" +
                        "              дюймов в сантиметры и наоборот.\n\n" +
                        "                               Версия: " + VERSION + "\n\n" +
                        "                             Created by Anec",
                "О программе", JOptionPane.PLAIN_MESSAGE));
        container.add(buttonAbout);

        for (Component c : this.getComponents()) {
            SwingUtilities.updateComponentTreeUI(c);
        }
    }
}
